# parts

## 74xx

* 74:    ttl 10ns
* 74LS:  low power schottky (ttl compatible) 30ns
* 74HCT: high speed cmos (ttl compatible) 12ns (but HC is also TTL compatible, just not according to the standard! technically it's TOO good)
* 74ACT: advanced cmos (ttl compatible) 10ns
* 74AHCT: advanced high speed cmos (ttl compatible) 5.5ns

* 74xx193:  presettable 4bit binary up/down counter (active edge triggered, leave unused pin high)
* 74xx573:  8bit tristate d-flipflop, transparent/level triggered load enable (xx574: same but edge-triggered load clock)
* 74xx541:  8bit tristate buffer/linedriver (xx540: inverted outputs)
* 74xx138:  3-to-8 decoder, multiple OEs (may act as 4-to-16 by using bit2 to OE either of 2 chips), one-cold (xx238: one-hot)
* 74xx4017: Johnson Decade Counter with 10 Decoded Outputs
* 74xx157:  quad 2-to-1 multiplexer
