# Mover

Yet another MOV-only CPU

# Core

this *only* does the actual MOV.

## General Archictecture

all bit widths are the same (except for the control bus)

* Outside accessible:
  * data bus
  * addr bus
  * 5bit control bus
    - CLK (input)
    - RST (input)
    - WR (output, device at addr should read from data bus)
    - RD (output, device at addr should write to data bus)
    - S0 (output, active in execution cycle 0)
* internal (unaddressable) registers:
  * register DATA
  * register ADDR
  * counter PC

## electrical conventions

* Data/Addr busses: VCC=`1`, GND=`0`, tristated:
  * if WR:
    * data+addr are outputs
  * if RD:
    * addr output
    * data input
  * otherwise High-Z
* control bus:
  * inputs are active low (CLK ticks on falling edge)
  * outputs are open drain/collector (=active low, inactive High-Z)

## memory map

```
(...): general purpose bus
0: entry point (PC gets set to 0 on reset)
   JUMP (when *writing* to 0, PC gets set to the data written)
```

## Instruction "Set"

there's no set. the only thing this CPU does is read 2 addresses and then copy data from one to the other.

`(X)` means "data in memory/peripheral at address `X`". aka indirection/pointer.

```
MOV a, b
  copies (a) to (b)

in memory they're interleaved (abababab...)
```

## Execution cycle

```
0: (PC) -> ADDR
1: Increment PC
2: (ADDR) -> DATA
3: (PC) -> ADDR
4: Increment PC
5: DATA -> (ADDR) / [if ADDR=0:] DATA -> PC
```

# reference implementation: Mover-NORROT-16

This is the example system, technically turing complete, which extends the core with an accumulator and logic.

Technically 2, because it's "logic part" is a bitwise universal gate.
But the NOR one is the one described here, considered the "reference".
For the other one simply replace all occurences of the word "NOR" with "NAND".

Also please hit me up when you figured out how to reinvent the universe so "XOR" becomes a viable candidate, that would spark joy!

* All words are 16bit.
* accumulator register
* NOR / bit rotation logic

## memory map

```
FFF0..FFFF: builtin peripherals
         F: NOP (write) / 0000h (read)
   2..   E: [reserved]
         1: NOR=(write) / Accumulator<rot<1 (read)
         0: Accumulator (read/write)
```

yes, that's turing complete:
  * no, it doesn't need a conditional jump.
    * just make your program rewrite its jump target as the condition's result.
  * the bitwise NOR is sufficient to implement any other logic gate
  * the bit rotation allows differently-positioned bits to interact
    (not even technically required for completeness, but I'm not gonna waste 7/8ths of all memory!)
