#include "Vtop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

#include <cstdint>
#include <iostream>
#include <memory>

void power_on_reset(const std::unique_ptr<Vtop> &top) {
	switch (top->contextp()->time()) {
			// power-on reset:
		case 0: top->nRST = 0; break;
		case 100: top->nRST = 1; break;
	}
}
void toggle_clock(const std::unique_ptr<Vtop> &top) {
	if (top->contextp()->time() % 10 == 0) top->CLK = !top->CLK;
}
void bus_access(const std::unique_ptr<Vtop> &top) {
	static uint16_t _ram[0x10000] = {
	 0x0002, //-0 -. <. ; jump [2]=6
	 0x0000, // 1  |  |
	 0x0006, //-2  |  | ; const 6
	 0x0000, // 3  |  | ; const 0
	 0xDEAD, //-4  |  | ; src
	 0xBEEF, // 5  |  | ; dest
	 0x0004, //-6 <'  | ; copy [4](src)
	 0x0005, // 7     | ; to [5](dest)
	 0x0003, //-8 ----' ; jump [3]=0
	 0x0000, // 9
	};

	if (!top->nRD) {
		top->Di = _ram[(size_t)top->A];
		std::cout << std::hex << "RD A: " << (int)top->A << " D: " << (int)top->Di << std::endl;
	} else if (!top->nWR) {
		_ram[(size_t)top->A] = top->Do;
		std::cout << std::hex << "WR A: " << (int)top->A << " D: " << (int)top->Do << std::endl;
	}
}

int main(int argc, char *argv[], char *env[]) {
	Verilated::mkdir("logs");

	const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};
#if VM_DEBUG
	contextp->debug(0);
#endif
	contextp->randReset(2);
	contextp->traceEverOn(true);
	contextp->commandArgs(argc, argv);

	const std::unique_ptr<Vtop> top{new Vtop{contextp.get(), "TOP"}};

	VerilatedVcdC *tfp = new VerilatedVcdC;
	top->trace(tfp, 0, 0);
	tfp->dumpvars(0, "");
	tfp->open("logs/sim.vcd");

	top->Di = 0;

	while (!contextp->gotFinish() && contextp->time() < 1000) {

		power_on_reset(top);
		toggle_clock(top);
		bus_access(top);

		top->eval();
		tfp->dump(contextp->time());
		contextp->timeInc(1);
	}
	top->final();
	tfp->close();

#if VM_COVERAGE
	contextp->coveragep()->write("logs/coverage.dat");
#endif
}
