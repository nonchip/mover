module Mover(
  input wire logic nRST,
  input wire logic CLK,
  input wire logic [15:0] Di,

  output wire logic nWR, nRD, nS0,
  output wire logic [15:0] A,
  output wire logic [15:0] Do
);

  wire logic _wr, _rd, _s0;

  open_drain_inv _od_wr(
    .in(_wr),
    .nOut(nWR)
  );

  open_drain_inv _od_rd(
    .in(_rd),
    .nOut(nRD)
  );

  open_drain_inv _od_s0(
    .in(_s0),
    .nOut(nS0)
  );

  core core (
    .clk     (!CLK),
    .reset   (!nRST),
    .wr      (_wr),
    .rd      (_rd),
    .s0      (_s0),
    .bus_addr(A),
    .bus_data_in(Di),
    .bus_data_out(Do)
  );

endmodule
