//(* blackbox *)
module tristate_dlatch_573( // like 74xx573
  input wire logic le, nOe,
  input wire bit [7:0] D,
  output wire logic [7:0] O
);

  tristate_dlatch _dl[7:0](
    .D(D),
    .O(O),
    .oe(!nOe),
    .le(le)
  );

endmodule
