//(* blackbox *)
module tristate_dlatch(
  input wire logic le, oe,
  input wire bit D,
  output wire logic O
);

  var reg val;

	always_latch
	  if(le)
			val = D;

  assign O = oe ? val : 'z;

endmodule
