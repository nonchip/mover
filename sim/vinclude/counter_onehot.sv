//(* blackbox *)
module counter_onehot #(
  parameter STATES = 8
) (
  input wire logic clk, load, oe,
  input wire bit [$clog2(STATES)-1 : 0] in,
  output wire bit [STATES-1:0] out
);

  wire bit [$clog2(STATES)-1:0] count;

  wire bit count_load;

  assign count_load = load || (count==STATES);

  counter #(
    .WIDTH($clog2(STATES))
  ) _counter (
    .oe(1'b1),
    .clk(clk),
    .in(in),
    .load(count_load),
    .out(count)
  );

  dec_onehot #(
    .WIDTH(STATES)
  ) _dec_onehot (
    .oe(oe),
    .in(count),
    .out(out)
  );

endmodule
