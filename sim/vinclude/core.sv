module core #(
  parameter BUS_WIDTH = 16
) (
  input wire logic reset, clk,
  output wire logic wr, rd, s0,
  output wire logic [BUS_WIDTH-1:0] bus_addr,
  input wire bit [BUS_WIDTH-1:0] bus_data_in,
  output wire logic [BUS_WIDTH-1:0] bus_data_out
);

  wire bit step_reset, step_clk, step_ptr_pc_to_addr, step_inc_pc, step_ptr_addr_to_data, step_data_to_ptr_addr, step0;
  execute_step step (.reset(step_reset), .clk(step_clk),	.ptr_pc_to_addr(step_ptr_pc_to_addr), .inc_pc(step_inc_pc), .ptr_addr_to_data(step_ptr_addr_to_data), .data_to_ptr_addr(step_data_to_ptr_addr),.step0(step0));

  wire logic [BUS_WIDTH-1:0] pc_out;
  wire logic [BUS_WIDTH-1:0] pc_in;
  wire bit pc_oe, pc_le, pc_clk;
  counter #(.WIDTH(BUS_WIDTH)) pc (.oe(pc_oe),.in(pc_in),.load(pc_le),.out(pc_out),.clk(pc_clk));

  wire logic [BUS_WIDTH-1:0] data_out;
  wire logic [BUS_WIDTH-1:0] data_in;
  wire bit data_oe, data_le;
  tristate_dlatch_573_n #(.WIDTH(BUS_WIDTH)) r_data(.D(data_in),.O(data_out),.nOe(!data_oe),.le(data_le));

  wire logic [BUS_WIDTH-1:0] addr_out;
  wire logic [BUS_WIDTH-1:0] addr_in;
  wire bit addr_oe, addr_le;
  tristate_dlatch_573_n #(.WIDTH(BUS_WIDTH)) r_addr(.D(addr_in),.O(addr_out),.nOe(!addr_oe),.le(addr_le));

  wire bit jump;
  assign jump = step_data_to_ptr_addr && addr_out == '0;

  assign step_reset = reset;
  assign step_clk = clk;

  wire bit gate_le = !clk;

  assign pc_in    = reset ? '0 : jump ? data_out: '0;
  assign pc_oe    = step_ptr_pc_to_addr;
  assign pc_le    = gate_le && (reset || jump);
  assign pc_clk   = step_inc_pc;

  assign data_in  = reset ? '0 : bus_data_in;
  assign data_oe  = step_data_to_ptr_addr;
  assign data_le  = gate_le && (reset || step_ptr_addr_to_data);

  assign addr_in  = reset ? '0 : bus_data_in;
  assign addr_oe  = step_ptr_addr_to_data || step_data_to_ptr_addr;
  assign addr_le  = gate_le && (reset || step_ptr_pc_to_addr);

  assign wr = data_oe && !jump;
  assign rd = step_ptr_addr_to_data || step_ptr_pc_to_addr;
  assign bus_addr = step_ptr_pc_to_addr ? pc_out : (addr_oe && !jump) ? addr_out : 'z;
  assign bus_data_out = wr ? data_out : 'z;

  assign s0 = step0;

endmodule
