//(* blackbox *)
module tristate_dlatch_573_n #(
  parameter WIDTH = 8
) (
  input wire logic le, nOe,
  input wire bit [WIDTH-1:0] D,
  output wire logic [WIDTH-1:0] O
);

  tristate_dlatch_573 _dl[(WIDTH/8)-1:0](
    .D(D),
    .O(O),
    .nOe(nOe),
    .le(le)
  );

endmodule
