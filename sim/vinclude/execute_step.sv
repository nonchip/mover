//(* blackbox *)
module execute_step(
  input wire logic reset, clk,
	output wire bit ptr_pc_to_addr, inc_pc, ptr_addr_to_data, data_to_ptr_addr, step0
);

  wire bit [5:0] step_bit;
  counter_onehot #(
    .STATES(6)
	) step_counter_decoder (
    .in('0),
    .oe(!reset),
    .clk(clk),
    .load(reset),
    .out(step_bit)
  );

  assign ptr_pc_to_addr   = !reset && (step_bit[0] || step_bit[3]);
  assign inc_pc           = !reset && (step_bit[1] || step_bit[4]);
  assign ptr_addr_to_data = !reset && (step_bit[2]);
  assign data_to_ptr_addr = !reset && (step_bit[5]);
  assign step0 = !reset && step_bit[0];

endmodule
