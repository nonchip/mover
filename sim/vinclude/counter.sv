//(* blackbox *)
module counter #(
  parameter WIDTH = 8 // counter bits
) (
  input wire logic clk, load, oe,
  input wire bit [WIDTH-1 : 0] in,
  output wire bit [WIDTH-1 : 0] out
);

  reg [WIDTH-1 : 0] val;

  always @(posedge clk or posedge load)
    if (load)
      val <= in;
    else
      val <= val + 1;

  assign out = oe ? val : '0;

endmodule
