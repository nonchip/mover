//(* blackbox *)
module open_drain_inv(
  input wire bit in,
  output wire logic nOut
);

`ifdef VERILATOR
  // the verilator is too dumb for top level tristate
  assign nOut = !in;
`else
  assign nOut = in?'0:'z;
`endif


endmodule
