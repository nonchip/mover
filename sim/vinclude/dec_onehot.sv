//(* blackbox *)
module dec_onehot #(
  parameter WIDTH = 2 // output bits
) (
  input wire logic oe,
  input wire bit [$clog2(WIDTH)-1:0] in,
  (* onehot *) output reg [WIDTH-1:0] out
);
  always @(in or oe) begin
    if (!oe)
      out = {WIDTH{1'b0}};
    else
      out = {{(WIDTH-1){1'b0}},{1'b1}} << in;
  end
endmodule
